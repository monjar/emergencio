import React from "react";
import "./Errors.css";
const ServerErrorPage = () => {
  return (
    <div class="errContainer">
      <div class="errorText">
        <h1>اوه اوه... خرابکاری شد</h1>
        <h4>سرور به مشکلی خورد ۵۰۰</h4>
      </div>
    </div>
  );
};
export default ServerErrorPage;
